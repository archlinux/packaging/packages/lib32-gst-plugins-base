This package has been absorbed by [`lib32-gstreamer`][gst].

[gst]: https://gitlab.archlinux.org/archlinux/packaging/packages/lib32-gstreamer
